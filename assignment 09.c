// open in write mode and write text, open in read mode and read the text in text file

#include <stdio.h>
#include <stdlib.h>


int main() {

    char arr1[200] = "UCSC is one of the leading institutes in Sri lanka for computing studies.";
    char arr2[200];
    char arr3[200] = " UCSC offers undergraduate and postgraduate level courses aiming a range of computing fields.";
    char arr4[500];

    FILE *fpointer;

    // open in write mode and write text to the file
    fpointer = fopen("assignment9.txt","w");

    if(strlen(arr1) > 0){
        fputs(arr1,fpointer);
    }
    fclose(fpointer);

    // open in read mode and read the text in text file
    fpointer = fopen("assignment9.txt","r");

    if(fgets(arr2,200,fpointer) != NULL){

        printf("%s\n",arr2);
    }
    fclose(fpointer);

    //append new sentence to the assignment9.txt
    fpointer = fopen("assignment9.txt","a");

    fputs(arr3,fpointer);

    fclose(fpointer);

    //read sentence and display on terminal
    fpointer = fopen("assignment9.txt","r");

    fgets(arr4,500,fpointer);

    printf("\nOutput after append:\n%s\n",arr4);

    fclose(fpointer);
}
